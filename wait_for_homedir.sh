#!/bin/bash

# shell script to potentially work around home directory mounting race
# conditions within pam

HOMEDIR=`getent passwd ${PAM_USER}| cut -f6 -d:`

LOG="/usr/bin/logger -t homedir-wait "

function log {
    /usr/bin/logger -t homedir-wait ${1}
}

log "Looking for ${HOMEDIR}"
for i in {1..5}; do
    ls ${HOMEDIR} &>/dev/null
    if [[ $? != 0 ]]; then
        log "Could not find ${HOMEDIR} trying again"
        /bin/sleep 1
    else
        log "Found ${HOMEDIR} continuing"
        exit 0
    fi
done
log "Could not find ${HOMEDIR} in time"
exit 0
