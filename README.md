## Description

Run the install script to install autofs and configure it to mount the users personal cifs space

Home directories will be available at /winhomes/$netid

## Requirements

* rapid-user (Or another way to configure authentication against WIN.DUKE.EDU for kerberos)

## Using as default home directory

By default, this module only makes the home directories available.  If you
would like to use them for the users actual home directy (what you get from
'getent passwd'), you must also include the following line in your pam
configuration:

```
session     optional     pam_exec.so debug seteuid /usr/local/bin/wait_for_homedir.sh
```

This will prevent a race condition of autofs caching an unavailable homedir on
first login

## SELinux might interfer and if it does run the following:
```
[root@host /]# semanage fcontext -a -e /home /winhomes
[root@host /]# restorecon -vR /winhomes