#!/bin/bash

INVENTORY=~/.rapid_ansible_hosts

echo localhost ansible_connection=local > $INVENTORY

if [ $# == 1 ]; then
  XUSERNAME=$1
  XUID=''
elif [ $# == 2 ]; then
  XUSERNAME=$1
  XUID=$2
fi

## Do the install here
ansible-playbook  -i $INVENTORY ./main.yaml | tee /tmp/rapid_image_status.txt

if [[ ${PIPESTATUS[0]} == 0 ]]; then
    echo "Ansible Playbook Succeeded"
    echo "success" > /tmp/rapid_image_complete
else
    echo "Ansible playbook failed"
    echo "fail" > /tmp/rapid_image_complete
fi
